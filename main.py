from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.optim.lr_scheduler import StepLR

import re
from random import sample
import more_itertools as mit
from models import *
import matplotlib.pyplot as plt
from utils import data,test,train

#Part1: Training hyperparameters
parser = argparse.ArgumentParser(description='PyTorch Image Classification Example')
parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                    help='input batch size for training (default: 64)')
parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                    help='input batch size for testing (default: 1000)')
parser.add_argument('--epochs', type=int, default=8, metavar='N',
                    help='number of epochs to train (default: 14)')
parser.add_argument('--lr', type=float, default=1.0, metavar='LR',
                    help='learning rate (default: 1.0)')
parser.add_argument('--gamma', type=float, default=0.7, metavar='M',
                    help='Learning rate step gamma (default: 0.7)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--dry-run', action='store_true', default=False,
                    help='quickly check a single pass')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--save-model', action='store_true', default=False,
                    help='For Saving the current Model')

parser.add_argument('--valid-size', type=float, default=0.50, metavar='split',
                    help='the ratio of training data/validation data: [0,1] (default: 0.90')    
parser.add_argument('--dataset', choices=['MNIST', 'CIFAR10', 'CIFAR100'],
                    default='MNIST')
args = parser.parse_args()

use_cuda = not args.no_cuda and torch.cuda.is_available()

torch.manual_seed(args.seed)

device = torch.device("cuda" if use_cuda else "cpu")

kwargs = {'batch_size': args.batch_size, 'valid_size': args.valid_size}
print('Check whether use_cuda: ',end='')
print(use_cuda)
if use_cuda:
    kwargs.update({'num_workers': 1,
                    'pin_memory': True,
                    'shuffle': True},
                    )

#Part2: preparing the data
print('==> Preparing data..')
trainset, testset, inputs, num_classes = data.getDataset(args.dataset)
train_loader, valid_loader, test_loader = data.getDataloader(trainset, testset, **kwargs)
examples = enumerate(test_loader)
batch_idx, (example_data, example_targets) = next(examples)
#print(example_data.shape)

fig = plt.figure()
for i in range(6):
    plt.subplot(2,3,i+1)
    plt.tight_layout()
    plt.imshow(example_data[i][0], cmap='gray', interpolation='none')
    plt.title("Ground Truth: {}".format(example_targets[i]))
    plt.xticks([])
    plt.yticks([])
plt.show()


#Part3: model and optimizer
print('==> Preparing model..')
# model = ResNet10(inputs, num_classes).to(device)
model = LeNet(inputs,num_classes).to(device)

# model = VGG('VGG19', inputs, num_classes).to(device)
# model = PreActResNet18(inputs, num_classes).to(device)
# model = GoogLeNet(inputs, num_classes).to(device)
# model = DenseNet121(inputs, num_classes).to(device)
# model = ResNeXt29_2x64d(inputs, num_classes).to(device)
# model = MobileNet(inputs, num_classes).to(device)
# model = MobileNetV2(inputs, num_classes).to(device)
# model = DPN92(inputs, num_classes).to(device)
# model = ShuffleNetG2() #has a problem
# model = SENet18(inputs, num_classes).to(device)
# model = ShuffleNetV2(1,inputs, num_classes).to(device)
# model = EfficientNetB0(inputs, num_classes).to(device)
# model = RegNetX_200MF(inputs, num_classes).to(device)

#dict_ = model.state_dict()
#torch.save(dict_,'orginal_all.pt')
optimizer = optim.Adadelta(model.parameters(), lr=args.lr)
#print(optimizer.state_dict())
scheduler = StepLR(optimizer, step_size=1, gamma=args.gamma)

#Part4: training, testing, and saving the losses
print('==> Start training..')
train_losses = []
train_counter = []
test_losses = []
test_counter = [0]
#test_counter = [i*math.floor(len(train_loader.dataset)*args.valid_size) for i in range(args.epochs + 1)]
test(model, device, test_loader, test_losses)
for epoch in range(1, args.epochs + 1):
    train(args, model, device, train_loader, optimizer, epoch, train_losses, train_counter, test_counter)
    test(model, device, test_loader, test_losses)
    scheduler.step()
#torch.save(train_counter,'train.pt')
#torch.save(test_counter,'test.pt')
print('==> Ploting the loss landscape..')
fig = plt.figure()
plt.plot(train_counter, train_losses, color='blue')
plt.scatter(test_counter, test_losses, color='red')
plt.legend(['Train Loss', 'Test Loss'], loc='upper right')
plt.xlabel('number of training examples seen')
plt.ylabel('negative log likelihood loss')
plt.show()

#Part5: Plotting the loss curves
print('==> Ploting model\'s few predictions..')
with torch.no_grad():
    output = model(example_data)

fig = plt.figure()
for i in range(6):
    plt.subplot(2,3,i+1)
    plt.tight_layout()
    plt.imshow(example_data[i][0], cmap='gray', interpolation='none')
    plt.title("Prediction: {}".format(
    output.data.max(1, keepdim=True)[1][i].item()))
    plt.xticks([])
    plt.yticks([])  
plt.show()

if args.save_model:
    torch.save(model.state_dict(), "mnist_cnn.pt")

#if __name__ == '__main__':
    #main()
import torch
import torch.nn.functional as F
import math
def train(args, model, device, train_loader, optimizer, epoch, train_losses, train_counter, test_counter):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), math.floor(len(train_loader.dataset)*args.valid_size),
                100. * batch_idx / len(train_loader), loss.item()))
            train_losses.append(loss.item())
            train_counter.append((batch_idx*64) + ((epoch-1)*math.floor(len(train_loader.dataset)*args.valid_size)))
            if args.dry_run:
                break
    test_counter.append(train_counter[-1])

import numpy as np
import torch
import torchvision
from torch.utils.data import Dataset
import torchvision.transforms as transforms
from torch.utils.data.sampler import SubsetRandomSampler
import matplotlib.pyplot as plt

#trainset = torchvision.datasets.CIFAR10(
    #root='./data', train=True, download=True, transform=transform_train)
#trainloader = torch.utils.data.DataLoader(
    #trainset, batch_size=128, shuffle=True, num_workers=2)

#testset = torchvision.datasets.CIFAR10(
    #root='./data', train=False, download=True, transform=transform_test)
#testloader = torch.utils.data.DataLoader(
    #testset, batch_size=100, shuffle=False, num_workers=2)

#classes = ('plane', 'car', 'bird', 'cat', 'deer',
           #'dog', 'frog', 'horse', 'ship', 'truck')


def getDataset(dataset):
    transform_mnist=transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.1307,), (0.3081,))
        ])

    transform_cifar = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
        ])

    #transform_train = transforms.Compose([
        #transforms.RandomCrop(32, padding=4),
        #transforms.RandomHorizontalFlip(),
        #transforms.ToTensor(),
        #transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    #])

    #transform_test = transforms.Compose([
        #transforms.ToTensor(),
        #transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    #])


    if(dataset == 'CIFAR10'):
        trainset = torchvision.datasets.CIFAR10(root='../data', train=True, download=True, transform=transform_cifar)
        testset = torchvision.datasets.CIFAR10(root='../data', train=False, download=True, transform=transform_cifar)
        num_classes = 10
        inputs=3

    elif(dataset == 'CIFAR100'):
        trainset = torchvision.datasets.CIFAR100(root='../data', train=True, download=True, transform=transform_cifar)
        testset = torchvision.datasets.CIFAR100(root='../data', train=False, download=True, transform=transform_cifar)
        num_classes = 100
        inputs = 3
        
    elif(dataset == 'MNIST'):
        trainset = torchvision.datasets.MNIST(root='../data', train=True, download=True, transform=transform_mnist)
        testset = torchvision.datasets.MNIST(root='../data', train=False, download=True, transform=transform_mnist)
        num_classes = 10
        inputs = 1

    return trainset, testset, inputs, num_classes


def getDataloader(trainset, testset, batch_size, valid_size=0.15, num_workers=0):
    num_train = len(trainset)
    indices = list(range(num_train))
    np.random.shuffle(indices)
    split = int(np.floor(valid_size * num_train))
    train_idx, valid_idx = indices[:split], indices[split:]

    train_sampler = SubsetRandomSampler(train_idx)
    valid_sampler = SubsetRandomSampler(valid_idx)

    train_loader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
        sampler=train_sampler, num_workers=num_workers)
    valid_loader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, 
        sampler=valid_sampler, num_workers=num_workers)
    test_loader = torch.utils.data.DataLoader(testset, batch_size=batch_size, 
        num_workers=num_workers)

    return train_loader, valid_loader, test_loader


def test():
    trainset, testset, inputs, num_classes = getDataset('MNIST')
    kwargs = {'batch_size': 64,'valid_size':0.10}
    
    train_loader, valid_loader, test_loader = getDataloader(trainset, testset, **kwargs)
    #print(len(train_loader))
    #for k,v in train_loader:
        #print(k)
        #print(len(v))
        
    #print(len(valid_loader))
    #print(len(test_loader))
    examples = enumerate(valid_loader)
    batch_idx, (example_data, example_targets) = next(examples)
    #print(example_data.shape)
    
    fig = plt.figure()
    for i in range(6):
        plt.subplot(2,3,i+1)
        plt.tight_layout()
        plt.imshow(example_data[i][0], cmap='gray', interpolation='none')
        plt.title("Ground Truth: {}".format(example_targets[i]))
        plt.xticks([])
        plt.yticks([])
    plt.show()

#test()
